# Which to use ?

## Input json & fasta
For the Input_fasta_json.py your 1st argument is the json and the 2nd one is the fasta, for example:
python3 Input_fasta_json Akihito.json teste.fasta 

## Input fasta randomized
For the Input_fasta_random_X.py your 1st argument is the fasta with random md and the 2nd one is the dataset_id and the 3rd is the column for example:
python3 Input_fasta_random.py teste.fasta d01 dna_seq_random_10 

## Input on table on column
For the Input_on_table_on_column your 1st argument is your data and the 2nd one is the table, the 3rd the dataset_id and the 4th the column name for example:
python3 insert_on_table_on_column.py android.svg trees d01 svg_ml_default



