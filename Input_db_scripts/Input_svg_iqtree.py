import sys
import psycopg2
from svg import insert_svg
from svg import connect
from svg import param_dict
from iqtree import insert_iqtree
import iqtree


if __name__ == "__main__":
    conn = connect(param_dict)
    with open(sys.argv[2]) as iqtree_file:
        iqtree = iqtree_file.readlines()
    with open(sys.argv[1]) as svg_file:
        svg = svg_file.readlines()
    insert_svg(conn, svg, 'trees')
    insert_iqtree(conn, iqtree, 'iqtree')
    conn.close()