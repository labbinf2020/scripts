#! /bin/bash

		#introduction to the program and requirements for a successful operation

            echo -e "\n"
            read -n 1 -s -r "The following script requires internet connection and the following packages: python3.6 , ncbi-entrez-direct for it to work. Please install it before using. This script will search and retrieve sequences using the NBCI Entrez API. Please write at prompt, followed by [ENTER] $CONTINUEKEY"
	    echo -e "\n"

		#the user manually inserts the arguments

	    echo -e "\n To download the FASTA file, please insert the Accession IDs of your interest."
            read  "ACCESSION ID: (example: 1712730555, ...)"

		#beginning of download mode (optional for the user)

            echo -e "\n\nYour file will be downloaded shortly...\n"; python3 -m webbrowser "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/?db=nucleotide&id=$1&rettype=fasta"; echo -e "\nFile downloaded successfully."; #into (~/Downloads/"$date"_fetched_sequences_of_"$1"_from_db_"$dbquery".fasta).\n"
		#end of download mode; credits and closing
            read -n 1 -s -r "Thank you for using this script. Created for Lab Bioinf in November 2020. Version 1.0  [PRESS ANY KEY TO CLOSE]    
"


