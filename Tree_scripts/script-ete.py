from ete3 import PhyloTree, TreeStyle
import numpy
import argparse

#### inputs:
#### input_file (-f) = sequências alinhadas em fasta
#### bipartitions_file (-j) = output bipartitions do RAxML
#### newick_file (-k) = output do iqtree do Bruno (tree file)

#### output:
#### visualização das árvores na shell e a criação de imagens no diretório em que se está a trabalhar

##################### Dar como input as variáveis que pretendemos #####################

PARSER = argparse.ArgumentParser()

PARSER.add_argument("-f", type=argparse.FileType(), dest="input_file", help="select input file")

PARSER.add_argument("-j", type=argparse.FileType(), dest="bipartitions_file", help="select bipartitions file")

PARSER.add_argument("-k", type=argparse.FileType(), dest="newick_file", help="select newick file")

ARGUMENTS = PARSER.parse_args()

##################### Construir as árvores filogenéticas ##########################

fasta_txt = "".join(ARGUMENTS.input_file.readlines())

tree_newick = PhyloTree("".join(ARGUMENTS.newick_file.readlines()), quoted_node_names=False, format=1)

print("Phylogenetic Tree Newick", tree_newick)

tree_raxml = PhyloTree("".join(ARGUMENTS.bipartitions_file.readlines()),alignment=fasta_txt, alg_format="fasta")

print ("Phylogenetic Tree RAxML", tree_raxml)

################ Visualizar as imagens das árvores #####################

raxml_txt = "".join(ARGUMENTS.bipartitions_file.readlines())

newick_txt = "".join(ARGUMENTS.newick_file.readlines())

tn = tree_newick
tr = tree_raxml

ts = TreeStyle()
ts.show_leaf_name = False
ts.show_branch_length = False
ts.show_branch_support = True
ts.scale =  300
ts.branch_vertical_margin = 10

tn.render("mytree_newick.svg", w=750, units="mm", tree_style=ts)
tr.render("mytree_raxml.svg", w=750, units="mm", tree_style=ts)


