# Script ETE

Este script tem como objetivo a visualização das árvores filogenéticas obtidas pelo RAxML e MrBayes de uma forma automatizada.
Este script tem como inputs:

input_file = sequências alinhadas em fasta

bipartitions_file = output bipartitions do RAxML

newick_file = output do iqtree do Bruno (tree file)

O output deste script vão ser duas imagens em formate SVG que estarão no diretório em que se corre o script.

Como correr este script?

Exemplo: python3 script-ete.py -f input_file -j bipartitions_file -k newick_file

# MrBayes Block

Este script tem como objetivo a incorporação do bloco do MrBayes dentro do mesmo.
Tem como input:

x = número de gerações que se pretende

y = grupo de teste ou outgroup

# Alinhamento, Converter Nexus, MrBayes, RAxML

Todos estes ficheiros em .txt são os comandos utilizados em cada programa.
