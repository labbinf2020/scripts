

def MrBayesBlock(ngen, outgroup):

	x = str(ngen) # Número de gerações

	y = str(outgroup) # Grupo de teste

	mrbayes = "begin mrbayes; set autoclose = yes; outgroup "+y+"; mcmcp ngen ="+x+" printfreq=1000 samplefreq=200 diagnfreq=1000 nchains=4 saverbrlens=yes filename=MyRun; mcmc; sumt filename=MyRun; end;"

	return mrbayes
