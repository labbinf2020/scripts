# Tree comparison

## Dependencies

- Install iqtree
- Install bioconvert
- **Pearl** (Included in ubuntu)

## Comparing fasta files


- Obtain fasta file and its Raxml.
- Run the iqtree command to obtain the treefile for the original tree (treefile is the output for the command below)

	` iqtree -s sequences_align.fasta -z RAxML_bipartitions.sequences -n 0 -zb 1000` 
	
- Like the first step you have to obtain the fasta file this time with the nulls/ missing values and its respective Raxml
- Run the same command as before but changing to the respective inputs in order to obtain its treefile

	`iqtree -s null_sequences_align.fasta -z RAxML_bipartitions.sequences -n 0 -zb 1000`

- Run N timesthe command to the corresponding number of files with different percentages of nulls
- Now we put together the various treefils obtained e write them to a treels file

	`cat sequences_align.fasta.treefile null_sequences_align.fasta.treefile > output.treels`

- Run for the last time the iqtree command in order to compare the different trees. (output.treels is the file obtained from last step while sequences_align.fasta is the first file for the original tree)

	`iqtree -s sequences_align.fasta -z output.treels -n 0 -zb 1000`


## Comparing nexus files:

- Very similar to fasta files;
- Obtain nexus file and its MrBayes(con.tre);
- Run shell script MrBayes2newick in which the first argument is the con.tre file and the second one is the newick file which will be used for the rest of the steps;
- Run iqtree command to obtain the treefile for the original tree;

	`iqtree -s sequences_align.nex -z output.newick -n 0 -zb 1000`

- Obtain nexus files with nulls/missing values and its MrBayes(con.tre)
- Just as before run the shell script MrBayes2newick;

- Run iqtree command changing the inputs in order to obtain the tree file of the sequence with nulls;

	`iqtree -s null_sequences_align.nex -z null_output.newick -n 0 -zb 1000`

- Run N times the command for the different percentages of missing values;
- Put all the treefiles obtained in one treels file;

	`cat sequences_align.nex.treefile null_sequences_align.nex.treefile > output.treels`

- Run one last time iqtree in order to obtain the comparison between the different trees.

	`iqtree -s sequences_align.nex -z output.treels -n 0 -zb 1000`
