#!/bin/bash
tmpfilename="_tmp.newick"
preformated=$(bioconvert nexus2newick $1 $tmpfilename)
formated=$(perl -pe 'BEGIN{ $/="&length" } s/\[&length/\n/g' $tmpfilename | sed 's/_mean.*}]//')
rm $tmpfilename
echo ${formated} > $2
