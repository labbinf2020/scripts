#!/user/bin/python3

'''
Script to repeat randomized and non-randomized n times.
'''

import sys
import os
import class_arguments
import randomized
    	
if __name__ == "__main__":
#Vai adicionar o argumento para repetir o ficheiro n vezes
    ARGS = class_arguments.ArgumentParser(sys.argv[1:])
    ARGS.PARSER.add_argument("-n", dest="number_repeat", type=int, help="Number of times to repeat"\
, required=True)
#Vai adicionar o argumento para guardar ficheiro .txt
    ARGS.PARSER.add_argument("-r", dest="save_file", type=str, help="Name for output file", required=True)
    SAVE_FILE = ARGS.argument_handler().save_file
#Para introduzir o ficheiro
    FASTA_HANDLE = ARGS.argument_handler().input_file
#Para introduzir a percentagem
    PERCENTAGE = ARGS.argument_handler().desired_percentage
#Para introduzir a seed
    SEED = ARGS.argument_handler().random_seed
    randomized.seed(SEED)
    SEQUENCES = randomized.fasta_parser.fasta_dictionary(FASTA_HANDLE)
#Introduz a percentagem de missings ás sequências
    ULTIMATE_DICTIONARY = randomized.inserting_missing_data(SEQUENCES, PERCENTAGE)
    file_name = SAVE_FILE + ".txt"
    if os.path.exists(file_name):
        append_write = "a"
    else:
        append_write = "w"      
    open_file = open(file_name, append_write)
    
#Para repetir o output obtido n vezes para um determinado seed (caso dado)
    for i in range(ARGS.argument_handler().number_repeat):
        randomized.fasta_writer(ULTIMATE_DICTIONARY)
    open_file.write(randomized.fasta_writer(ULTIMATE_DICTIONARY) + "\n")
    open_file.close()
