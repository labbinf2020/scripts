import argparse

#Definição dos argumentos usando o argparse
class ArgumentParser(object):
    def __init__(self, arg_list):
        self.arg_list = arg_list
        self.PARSER = argparse.ArgumentParser()
#Argumento para introduzir o ficheiro
        self.PARSER.add_argument("-f", type=argparse.FileType(), dest="input_file", required=True, help="select input file")
#Argumento para introduzir um seed
        self.PARSER.add_argument("-s", type=int, dest="random_seed", help="an integer as the seed for the random function")
#Argumento para introduzir a percentagem de missing values
        self.PARSER.add_argument("-p", type=int, dest="desired_percentage", required=True, help="an integer for the desired random percentage")
        
        
    def argument_handler(self):
        self.ARGUMENTS = self.PARSER.parse_args(self.arg_list)
        return self.ARGUMENTS
