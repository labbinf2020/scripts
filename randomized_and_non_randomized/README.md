# Randomized

The randomized.py is a script made to insert missing data in fasta sequences. 
It takes a Fasta sequence as input (a fasta file as command line arguments) and transforms it into a python dictionary, using the fasta_parser.py script, then it takes that dictionary and goes through every single codon and for each one a random number between 1 and 100 is generated. If the number is less or equal to the desired missing data percentage it will change that codon to missing data(N).
For example, if we want 10% missing data every time the random value is between 1-10 those codons will be missing data.
The output is sent to stdout for flexibility purposes.
Note: there’s also a seed function that can be an input (command line arguments) that will make all the same random values every time, for each seed, for reproducibility purposes.

## How to run

- To run

    `python3 randomized.py`

- Required arguments:

    `-f` input file

    `-p` desired percentage
    
    `-s` random seed (optional)

- If you need help:

    `python3 randomized.py -h`


# Multiplex

The multiplex.py run randomized and non-randomized scripts n times and save them in a txt file.

## How to run

- To run it has to be in the repository where the scripts are located.

    `python3 multiplex.py`

- Required arguments

    `-f` file name

    `-p` percentage
    
    `-n` number of times to repeat
    
    `-r` file name to save the output

- If you need help

    `python3 multiplex.py -h`
