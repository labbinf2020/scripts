## For the Input to DB folder

In this folder you can find all the scripts with the respective mockups that are going to be used for automatic input on your local db or on our docker.
Be aware with the parameters and change them to the right ones as they are defaulted for me local DB for testing

## For the Tree Comparison folder

This folder contains the script used to compare the different trees obtained. Don't forget to change the parameters for whichever files you have.

## For the Randomized and Non Randomized scripts folder
In this folder are stored all the scripts related to the random insertion of missing data in fasta sequences and all the multiplex scripts.

## For the Tree scripts folder

In this folder you can find the commands and scripts provided for the alignment, realization and visualization of phylogenetic trees.
